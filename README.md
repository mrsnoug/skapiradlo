# Skąpiradło

It is a project developed to help with shopping optimization. 

IMPORTANT: It was developed FOR Windows OS.

## Tech summary

It was developed with:

* Python 3.8 64-bit
* Django 2.2.7
* Celery 4.3.0
* Redis 3.2.100

Tested on 64-bit Windows 10

## Requirements

Apart from installing Python 3.8 following external python modules are required:

`pip install django`

`pip install requests`

`pip install bs4`

`pip install celery`

`pip install redis`

## Additional required software

Download Redis server (zip file) from: https://github.com/microsoftarchive/redis/releases/tag/win-3.2.100

Then unzip it somewhere (we suggest that you put __Redis-x64-3.2.100__ directory inside the same directory where this README file is in)

## Running the project

1. Start Redis Server (_redis-server.exe_ file)
2. Then run _redis-cli.exe_ and in the new window type `ping`. The response should be `PONG`. If that's the case then Redis is working correctly
3. Run the command line and browse to the directory with manage.py inside. Then run that command:

 `celery worker -A skapiradlo --loglevel=info`

4. In a different cli (but from the same directory) run:

 `python manage.py runserver`

5. The application should now be running on localhost:8000

NOTE: This project accepts requests from localhost. This means that it is only possible to use it on the same machine it is running on. This is the default behaviour and if a user wishes to make it available via network, ALLOWED_HOSTS variable in settings.py needs to be modified.

DISCLAIMER:

It is possible to port this application to other OS, however it will require a change in Message Broker settings. Master branch is written to use Redis server for Windows OS. If the user wishes to use this application on a different OS, developers ARE NOT responsible for any issues or crashes (although none are expected if everything would be set up properly).

## Known Issues

Measured performance of the application met the project assumptions. However one crucial part of application's operation is beyond optimization. If there will be any network issues or Skapiec.pl servers will not respond as quick as they did during our "internal" tests (almost perfect conditions) user might encounter some performance issues.

Also it is worth noting that if a processor is busy with something else it might slow down the application, because this project uses Celery as the job scheduler.
