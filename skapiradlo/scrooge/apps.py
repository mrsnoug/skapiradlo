from django.apps import AppConfig


class ScroogeConfig(AppConfig):
    name = 'scrooge'
