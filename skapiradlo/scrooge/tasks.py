import pickle

from bs4 import BeautifulSoup
from celery import shared_task
from django.core.serializers import deserialize

from .scrooge_classes.data_processor import DataProcessor
from .scrooge_classes.logger import Logger
from .scrooge_classes.skapiec_api_handler import SkapiecAPIHandler
from .scrooge_classes.utils import ScroogeUtils


@shared_task
def process_data(items_pickle: list):
    """
    This method is responsible for all tasks regarding acquiring and 
    processing data from Skapiec.pl. It is a Celery Task, so will be 
    performed when the resources are free --> it won't block the User's 
    interaction with the website. 

    In here Methods from SkapiecAPIHandler and DataProcessor are invoked.
    """

    final_result = []
    items = []

    logger = Logger()

    for pick in items_pickle:
        # unpickling, celery does not allow for data transition in a plain Python Object format
        items.append(pickle.loads(pick))

    for item in items:
        logger.append_to_log(item.name)
        logger.append_to_log(item.set_and_get_timestamp())

        # this is the first query for a given Item so url for that query will be logged
        # search_soup = SkapiecAPIHandler.send_search_request_from_item(item)
        if (search_soup := SkapiecAPIHandler.send_search_request_from_item(item)) is None:
            logger.append_to_log("ERROR: Skapiec did not return OK satus code")
            logger.save_log_to_file()
            continue
        
        logger.append_to_log(item.url)

        item_result = []
        if DataProcessor.check_if_user_is_fantastic(search_soup):
            # user is not fantastic and wanted to break our app ;c
            # we just skip this item and log it
            logger.append_to_log("ERROR: User is not fantastic")
            logger.save_log_to_file()
            continue

        if DataProcessor.check_if_response_is_single_product(search_soup):
            # if user provides very specific item name Skapiec may return
            # product page right away
            item_result.append(
                DataProcessor.find_best_deal_from_product_site(search_soup, item)
            )
        else:
            # first looking for the most interesting products, Skapiec.pl
            # returns pretty decent list so it is basically checking if the
            # price is inside boundaries given by user
            potentially_good = DataProcessor.find_best_product_from_search(
                search_soup, item
            )
            soups = []
            for maybe in potentially_good:
                if maybe[0]:
                    # There is only one seller for that product
                    soup_for_single = SkapiecAPIHandler.send_request_from_url(maybe[1])
                    single_res = DataProcessor.find_best_deal_from_product_site(
                        soup_for_single, item
                    )
                    if len(single_res) > 0:
                        # if not even a single result was found this check is needed
                        # to make sure there won't be any index out of range
                        item_result.append(single_res[0])
                else:
                    # There are multiple sellers for that product
                    soup_for_multi = SkapiecAPIHandler.send_request_from_url(maybe[1])
                    multi_res = DataProcessor.find_best_deal_from_product_site(
                        soup_for_multi, item
                    )
                    for res in multi_res:
                        item_result.append(res)
                        if len(item_result) >= 3:
                            break

                if len(item_result) >= 3:
                    # if we already fitted the result with 3 best offers we stop the processing
                    break

        logger.append_to_log(item_result)
        logger.append_to_log(ScroogeUtils.calculate_delta_time(item.timestamp))
        logger.save_log_to_file()

        final_result.append([item, item_result])

    # we append that dashed line to the log for convenience. Chunks of requests from
    # different requests are now separated.
    logger.append_to_log("--------------------------------------------------")
    logger.save_log_to_file()
    return final_result
