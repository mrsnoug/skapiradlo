import pickle

from celery import current_app
from django.core.serializers import deserialize, serialize
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render

from .forms import ItemForm
from .scrooge_classes.item import Item
from .scrooge_classes.scrooge_exceptions import (
    InvalidItemException,
    MinGreaterThanMaxException,
    PricesBelowZeroException,
    QuantityNotInBoundariesException,
    ReputationNotInBoundariesException,
)
from .tasks import process_data


def redirect_to_request(request):
    return redirect("/make_a_request")


def request_page(request):
    """
    View responsbile for serving forms. User just fills the data he/she
    want to receive. Based on request method (either GET or POST) this 
    view gets the data from forms or renders them. If something was wrong
    with forms provided by user then this view renders a fresh set.
    """

    if request.method == "POST":
        # /make_a_request/ page is organised in such a way that there are
        # five forms present at all times, but not all of them may be vi-
        # sible. Each item field will have its own separate list
        item_name_list = request.POST.getlist("item_name")
        quantity_list = request.POST.getlist("quantity")
        min_price_list = request.POST.getlist("min_price")
        max_price_list = request.POST.getlist("max_price")
        min_reputation_list = request.POST.getlist("min_reputation")

        items = []

        for i in range(5):
            try:
                item = Item.validate_item(
                    item_name_list[i],
                    quantity_list[i],
                    min_price_list[i],
                    max_price_list[i],
                    min_reputation_list[i],
                )
                items.append(item)
            except InvalidItemException:
                if len(items) > 0:
                    # If someone filled for example 2 Item Forms then this
                    # exception will be raised on the third item, so we need
                    # to process just first two
                    break
                else:
                    form_list = [
                        ItemForm(),
                        ItemForm(),
                        ItemForm(),
                        ItemForm(),
                        ItemForm(),
                    ]
                    messages = ["BŁĄD: Przedmiot został wpisany niepoprawnie"]
                    return render(
                        request=request,
                        template_name="scrooge/home.html",
                        context={"form_list": form_list},
                    )

            except MinGreaterThanMaxException:
                messages = ["BŁĄD: Cena minimalna jest większa od maksymalnej"]
                form_list = [ItemForm(), ItemForm(), ItemForm(), ItemForm(), ItemForm()]
                return render(
                    request=request,
                    template_name="scrooge/home.html",
                    context={"form_list": form_list, "messages": messages},
                )

            except ReputationNotInBoundariesException:
                messages = ["BŁĄD: Reputacja musi zawierać się w przedziale <1, 5>"]
                form_list = [ItemForm(), ItemForm(), ItemForm(), ItemForm(), ItemForm()]
                return render(
                    request=request,
                    template_name="scrooge/home.html",
                    context={"form_list": form_list, "messages": messages},
                )

            except QuantityNotInBoundariesException:
                messages = ["BŁĄD: Liczba sztuk musi zawierać się w przedziale <1, 99>"]
                form_list = [ItemForm(), ItemForm(), ItemForm(), ItemForm(), ItemForm()]
                return render(
                    request=request,
                    template_name="scrooge/home.html",
                    context={"form_list": form_list, "messages": messages},
                )

            except PricesBelowZeroException:
                messages = ["BŁĄD: Ceny muszą być dodatnie"]
                form_list = [ItemForm(), ItemForm(), ItemForm(), ItemForm(), ItemForm()]
                return render(
                    request=request,
                    template_name="scrooge/home.html",
                    context={"form_list": form_list, "messages": messages},
                )

            except ValueError:
                messages = ["BŁĄD: nie została podana wartość całkowita"]
                form_list = [ItemForm(), ItemForm(), ItemForm(), ItemForm(), ItemForm()]
                return render(
                    request=request,
                    template_name="scrooge/home.html",
                    context={"form_list": form_list, "messages": messages},
                )

        if len(items) > 0:
            # This if is needed because if somehow none item object was created,
            # i.e.: Not a single form was created properly, and we ended up here

            items_pickle = []
            for item in items:
                item_pickle = pickle.dumps(item)
                items_pickle.append(item_pickle)

            task = process_data.delay(items_pickle)
            task_id = task.id
            return render(
                request=request,
                template_name="scrooge/loading.html",
                context={"task_id": task_id},
            )

    form_list = [ItemForm(), ItemForm(), ItemForm(), ItemForm(), ItemForm()]
    return render(
        request=request,
        template_name="scrooge/home.html",
        context={"form_list": form_list},
    )


def loading_page(request, task_id):
    return render(
        request=request,
        template_name="scrooge/loading.html",
        context={"task_id": task_id},
    )


def output_page(request, task_id):
    """
    Output page. I gets the result from celery task and prepares a context.
    """

    task = current_app.AsyncResult(task_id)

    if task.status == "SUCCESS":
        # double check if it was successful, if someone stole task_id and
        # tried to manually get to this page he/she will be redirected to
        # the loading page for that task_id
        data = task.get()
    else:
        return render(
            request=request,
            template_name="scrooge/loading.html",
            context={"task_id": task_id},
        )

    context = {}

    for i in range(len(data)):
        # dynamically create context data based on the celery task result

        single_item_data = data[i]
        item_key = "item" + str(i)
        context[item_key] = single_item_data[0]
        single_item_results_set = single_item_data[1]
        item_result_key = item_key + "_results"
        context[item_result_key] = single_item_results_set

    return render(request=request, template_name="scrooge/output.html", context=context)


def check_output(request, task_id):
    """
    Method that will return either HTTP 200, HTTP 204 or HTTP 500 depending on the Celery Task
    status. If the Task will not be finished it returns 204 (JavaScript on the frontend will 
    stay on the loading page). If the Task will be finished, i.e.: results are ready, then 
    JavaScript will change window.location.href to the output page, where the results will
    be returned. However if Celery task ended in failure then the response code is 400.
    """

    task = current_app.AsyncResult(task_id)
    response = HttpResponse()

    if task.status == "SUCCESS":
        response.status_code = 200
    elif task.status == "FAILURE":
        response.status_code = 500
    else:
        response.status_code = 204

    return response
