import urllib

from bs4 import BeautifulSoup
from requests import get

from .item import Item

BASE_URL = "http://skapiec.pl"
SEARCH_URL = BASE_URL + "/szukaj/w_calym_serwisie/"
BASE_URL_DELIVERY = BASE_URL + "/delivery.php"


class SkapiecAPIHandler:
    """
    class handling all communication with Skapiec.pl

    Attributes
    -----------
    base_url : str
        It contains the target website domain
    serach_url : str
        It contains the url send in search of an Item

    Methods
    -----------
    send_search_request_from_item(item):
        Sends a search request to a targer website and then transforms
        the response into BeautifulSoup object and returns it. It uses 
        Item and SEARCH_URL. It uses HTTP GET.
    send_request_from_url(url):
        Send the HTTP GET request to the url and return BeautifulSoup
        html parser from a response.
    """

    @staticmethod
    def send_search_request_from_item(item: Item) -> BeautifulSoup:
        # print(item)
        item_name = item.name
        name_url_encoded = urllib.parse.quote_plus(item_name)

        url_to_send = SEARCH_URL + name_url_encoded
        item.url = url_to_send
        response = get(url_to_send)
        if not response.ok:
            return None

        return BeautifulSoup(response.text, "html.parser")

    @staticmethod
    def send_request_from_url(url: str) -> BeautifulSoup:
        response = get(url)
        return BeautifulSoup(response.text, "html.parser")
