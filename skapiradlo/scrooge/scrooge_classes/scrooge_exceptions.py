class InvalidItemException(Exception):
    """ Raised if any of the required fields are None (in factory method) """

    pass


class MinGreaterThanMaxException(Exception):
    """ Raised if min_price is greater then max_price (in factory method) """

    pass


class ReputationNotInBoundariesException(Exception):
    """ Raised if reputation provided is > 5 or < 1 """

    pass


class QuantityNotInBoundariesException(Exception):
    """ Raised if quantity provided is > 99 or < 1 """

    pass


class PricesBelowZeroException(Exception):
    """ Raisef if either min_price or max_price is lesser then 0 """

    pass
