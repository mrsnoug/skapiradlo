from datetime import datetime

from bs4 import BeautifulSoup, Tag

from .skapiec_api_handler import BASE_URL_DELIVERY, SkapiecAPIHandler


class ScroogeUtils:
    """
    Plain old Utils class. If something needs to be done in more than one
    place in App it should be done here.

    Methods
    -----------
    price_from_string(price_string):
        Method that converts Skapiec.pl prices to float number and returns it.
        It gets rid of all non-digit characters that may be present in the price_string.
    limit_to_currency(price):
        Simple method for converting float price into string and limiting it to
        two decimal places.
    check_other_delivery_type_available(menu):
        It looks through the html of the delivery screen and tries every single
        one. If the delivery is available then it instantly returns that specific
        html.
    calculate_delta_time(time1)::
        Returns string representation of delta time between time provided in method
        argument and datetime.datetime.now(). 
    """

    @staticmethod
    def price_from_string(price_string: str) -> float:
        price = (
            price_string.replace("\n", "")
            .replace("zł", "")
            .replace("od", "")
            .replace(" ", "")
            .replace(",", ".")
        )

        return float(price)

    @staticmethod
    def limit_to_currency(price: float):
        return "{0:.2f}".format(price)

    @staticmethod
    def check_other_delivery_type_available(menu: Tag) -> list:
        other_delivery_types = []
        for li in menu.find_all("li"):
            if li.get("class") is None:
                url = BASE_URL_DELIVERY + li.find("a").get("href")
                delivery_response = SkapiecAPIHandler.send_request_from_url(url)
                body = delivery_response.find("body")
                delivery_data = body.find("div", {"id": "switcher1"}).find(
                    "table", {"id": "deliveryRulesets"}
                )

                if (
                    "Wybrana metoda dostawy nie jest dostępna dla tego produktu"
                    not in delivery_data.text
                ):
                    return (True, delivery_data)

        # Returning tuple of two Falses due to the checks done in processing code
        return (False, False)

    @staticmethod
    def calculate_delta_time(time1: datetime):
        return str(datetime.now() - time1)
