import ast

from bs4 import BeautifulSoup

from .item import Item
from .skapiec_api_handler import BASE_URL, SkapiecAPIHandler
from .utils import ScroogeUtils


class DataProcessor:
    """
    Class handling all data manipulation and processing. It looks for best
    deals etc.

    Methods
    -----------
    check_if_user_is_fantastic(soup):
        Retruns False if user is not fantastic and wants to break our app.
        User is not fantastic if he/she wants to look for something that
        does not exist in Skapiec.pl. For example, user is not fantastic
        if he/she wants to look for item called 'fsekfnkaenla'.
        NOTE: This method may be hurtful for nordic users
    check_if_response_is_single_product(soup):
        Checks if a specific <div> is present in the soup. If it is then
        it means that it's the site for a product, i.e.: one can look for
        the store deals in there. If that <div> is not present, then 
        find_best_product_from_search(soup) method should be called.
    find_best_product_from_search(soup):
        Method for extracting best product searched. Takes a BeautifulSoup
        object as argument and finds the product in HTML. 
    find_best_deal_from_product_site(soup):
        Extracts best deal from a product site. Looks for the best sellers
        in html. It checks if the sufficient ammount of reviews are present
        (at least 20), checks if price is insie boundaries provided by user
        and also looks for delivery price. If delivery price was not fetched
        for various reasons then that offer is skipped.
    """

    @staticmethod
    def check_if_user_is_fantastic(soup: BeautifulSoup) -> bool:
        if soup.find("div", {"class": "message only-header info"}) is None:
            return False

        return True

    @staticmethod
    def check_if_response_is_single_product(soup: BeautifulSoup) -> bool:
        product = soup.find_all("div", {"class": "product"})
        if len(product) == 0:
            return False

        return True

    @staticmethod
    def find_best_product_from_search(soup: BeautifulSoup, item: Item) -> list:
        # Looking for that list of items first
        form_with_products = soup.find_all(name="form", attrs={"class": "wrapper"})

        tag = form_with_products[0]
        products = tag.find_next("div").find_all("div", {"class": "box-row js"})

        # in potentially_good_ones list all items that meet price criteria will be added
        potentially_good_ones = []
        for product in products:
            tags = product.find_all("a")
            price = None
            single_seller = None

            if product.find("a", {"class": "more-info"}):
                # there is a single seller
                price = tags[1].find("strong").text
                single_seller = True
            else:
                # there are multiple sellers
                price = tags[0].find("strong").text
                single_seller = False

            price = ScroogeUtils.price_from_string(price)

            if price >= item.price_min and price <= item.price_max:
                product_site = tags[0].get("href")
                to_append = (single_seller, BASE_URL + product_site)
                potentially_good_ones.append(to_append)

        return potentially_good_ones

    @staticmethod
    def find_best_deal_from_product_site(soup: BeautifulSoup, item: Item) -> list:
        result = []

        # First looking for the right html tags
        offers = (
            soup.find_all("div", {"class": "product"})[0]
            .find_all("div", {"class": "content-wrapper"})[1]
            .find_next("div", {"class": "js page prices"})
        )

        skapiec_says_good = offers.find_next("div", {"class": "offers-list promo js"})
        others = offers.find_next("div", {"class": "offers-list all js"})

        # Offers are in two unordered lists (promoted offers and not promoted), we
        # need to check all of them. The way Skapiec.pl is constructed is weird...
        # In fact every other <li> is what we want
        li_s_good = skapiec_says_good.find_next("ul").find_all("li")
        list_of_best = [li_s_good[i] for i in range(len(li_s_good)) if i % 2 == 0]

        # combining all offers into one list for simplicity
        all_offers = []
        for li in list_of_best:
            all_offers.append(li)

        if others is not None:
            # if there is only one offer then others variable will be None
            li_s_others = others.find_next("ul").find_all(
                "li", {"data-promoted": "false"}
            )

            list_of_others = [
                li_s_others[i] for i in range(len(li_s_others)) if i % 2 == 0
            ]
            for li in list_of_others:
                all_offers.append(li)

        for best in all_offers:
            # all things inside a single offer are also inside <ul>
            if (is_it_add := best.get("class")) is not None:
                if is_it_add[0] == "ad":
                    # sometimes there are ads
                    continue

            # details is one 'row' with an offer
            details = best.find_next("li").find_next("a")

            opinions = (
                details.find_next("object", {"class": "section-opinions"})
                .find_next("div")
                .get("data-description")
            )

            if opinions is None:
                # For some reason no opinions are provided. That is the case if
                # the seller is 'external', for example: amazon.de
                continue

            opinions_data = ast.literal_eval(opinions)

            if opinions_data["count"] < 20 or opinions_data["avg"] < item.min_reputation:
                # This seller is not creadible enough
                continue

            url = BASE_URL + details.get("href")
            price = details.find_next("object", {"class": "section-price-value"}).text
            price = ScroogeUtils.price_from_string(price)

            if price > item.price_max or price < item.price_min:
                # offer not inside price boundaries so skip that one
                continue

            delivery_price = details.find_next("object", {"class": "section-price-cost"})

            if "Darmowa dostawa" in delivery_price.text:
                delivery_price = 0.0
            else:
                to_query_for_delivery = delivery_price.find_next("a").get("href")

                if "http" in to_query_for_delivery:
                    # this means that it is an url pointing to an external shop page.
                    # We don't like external shops so **skip** that one
                    continue

                to_query_for_delivery = BASE_URL + to_query_for_delivery
                delivery_response = SkapiecAPIHandler.send_request_from_url(
                    to_query_for_delivery
                )
                body = delivery_response.find("body")

                if body.find("div", {"class": "typeswitch"}):
                    menu = body.find("ul", {"class": "menu"})

                    delivery_data = body.find("div", {"id": "switcher1"}).find(
                        "table", {"id": "deliveryRulesets"}
                    )

                    if (
                        "Wybrana metoda dostawy nie jest dostępna dla tego produktu"
                        in delivery_data.text
                    ):
                        # We need to see if other types of delivery are ok. If that is the case
                        # we just replce old delivery_data with the one that contains prices
                        other_delivery_types = ScroogeUtils.check_other_delivery_type_available(
                            menu
                        )
                        if other_delivery_types[0] == True:
                            delivery_data = other_delivery_types[1]
                        else:
                            continue

                    # prices are presented in a table with alternating background colors
                    # Skapiec marks them with either class "odd" or "even", we need to
                    # consider both as those are just different payment options (i.e.:
                    # credit card, cash on delivery etc.)
                    delivery_prices_set1 = delivery_data.find_all("tr", {"class": "odd"})
                    delivery_prices_set2 = delivery_data.find_all("tr", {"class": "even"})

                    # Combining all prices into one list
                    delivery_prices_set = []
                    for tr in delivery_prices_set1:
                        delivery_prices_set.append(tr)

                    for tr in delivery_prices_set2:
                        delivery_prices_set.append(tr)

                    # looking for all the prices and picking the lowest one
                    possible_delivery_prices = []
                    for tr in delivery_prices_set:
                        price_str = tr.find("div").find("b").text
                        current_delivery_price = ScroogeUtils.price_from_string(price_str)
                        possible_delivery_prices.append(current_delivery_price)

                    if len(possible_delivery_prices) > 0:
                        delivery_price = min(possible_delivery_prices)

                    else:
                        # skipping that offer if there were no delivery price added
                        # This is needed because during tests it still sometimes got
                        # to this point
                        continue
                else:
                    # skipping seller, because delivery data is not available
                    continue

            total_price = price * item.quantity + delivery_price

            seller_name = (
                details.find_next("object", {"class": "section-shop-logo"})
                .find_next("img")
                .get("alt")
            )

            price = ScroogeUtils.limit_to_currency(price)
            delivery_price = ScroogeUtils.limit_to_currency(delivery_price)
            total_price = ScroogeUtils.limit_to_currency(total_price)

            single_result = [price, delivery_price, url, total_price, seller_name]
            result.append(single_result)

        # sorting based on fourth list element, which is total_price, i.e.: results with
        # smallest total_price will be first in the list
        result.sort(key=lambda x: x[3])

        return result
