import os
import urllib

from django.conf import settings


class Logger:
    """
    Simple class for basic logging. 

    Attributes
    -----------
    line : str
        String containing logs for a single Item
    file : str
        String containing path to the log file

    Methods 
    -----------
    append_to_log(to_append)
        Adds a new information to the line (string concatenation).
    save_log_to_file()
        Saves the line into the file (prepending '\n' first) and also sets
        line to empty string to make it ready for next Item.
    """

    line = ""
    file = "path/to/log/file.log"
    info_separator = "---"

    def __init__(self):
        self.file = os.path.join(settings.BASE_DIR, "scrooge/Logs/logs.log")

    def append_to_log(self, to_append):
        self.line = "".join([self.line, str(to_append), self.info_separator])

    def save_log_to_file(self):
        # opening file for appending
        f = open(self.file, "a", encoding="utf-8")
        f.write("".join([self.line, "\n"]))
        f.close()
        self.line = ""
